"use client";
import { useQuery } from "@tanstack/react-query";
import React from "react";

const Test = () => {
  const fetchtodos = async () =>
    await fetch("https://jsonplaceholder.typicode.com/todos?page=1").then(
      (res) => res.json(),
    );

  const { isPending, isError, data, error } = useQuery({
    queryKey: ["todo"],
    queryFn: fetchtodos,
  });
  if (isPending) {
    return <span>Loading...</span>;
  }

  if (isError) {
    return <span>Error: {error.message}</span>;
  }

  console.log(data);
  return (
    <div>
      {data.map((item) => (
        <p key={item.id}>{item.title}</p>
      ))}
    </div>
  );
};

export default Test;
