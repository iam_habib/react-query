import Todos from "@/components/todos";
import {
  HydrationBoundary,
  QueryClient,
  dehydrate,
} from "@tanstack/react-query";
import React from "react";

const Server = async () => {
  const queryClient = new QueryClient();

  const fetchtodos = async () =>
    await fetch("https://jsonplaceholder.typicode.com/todos?page=1").then(
      (res) => res.json(),
    );

  await queryClient.prefetchQuery({
    queryKey: ["posts"],
    queryFn: fetchtodos,
  });

  const data = queryClient.getQueryData(["posts"]);

  return (
    <HydrationBoundary state={dehydrate(queryClient)}>
      <Todos data={data} />
    </HydrationBoundary>
  );
};

export default Server;
