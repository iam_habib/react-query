import React from "react";

const Todos = ({ data }) => {
  return (
    <div>
      {data.map((item) => (
        <p key={item.id}>{item.title}</p>
      ))}
    </div>
  );
};

export default Todos;
